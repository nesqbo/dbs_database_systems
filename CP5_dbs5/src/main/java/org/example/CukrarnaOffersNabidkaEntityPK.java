package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class CukrarnaOffersNabidkaEntityPK implements Serializable {
    public CukrarnaOffersNabidkaEntityPK() {
    }
    @Id
    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "ulice", referencedColumnName = "ulice"),
            @JoinColumn(name = "psc", referencedColumnName = "psc"),
            @JoinColumn(name = "cp", referencedColumnName = "cp")
    })
    private CukrarnaEntityPK cukrarna;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal verze;



    public CukrarnaEntityPK getCukrarna() {
        return cukrarna;
    }

    public BigDecimal getVerze() {
        return verze;
    }

    public void setVerze(BigDecimal verze) {
        this.verze = verze;
    }

    public void setCukrarna(CukrarnaEntityPK cukrarna) {
        this.cukrarna = cukrarna;
    }

    @Override
    public int hashCode() {
        return Objects.hash(verze, cukrarna);
    }
}
