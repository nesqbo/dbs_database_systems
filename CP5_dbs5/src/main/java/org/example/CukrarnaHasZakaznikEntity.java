package org.example;
import jakarta.persistence.*;

import java.util.Objects;


@Entity
@Table(name = "cukrarna_has_zakaznik")
@IdClass(CukrarnaHasZakaznikEntityPK.class)
public class CukrarnaHasZakaznikEntity {

    public CukrarnaHasZakaznikEntity() {
    }

    @Id
    @ManyToOne
    @JoinColumns({
            @JoinColumn(name = "ulice", referencedColumnName = "ulice"),
            @JoinColumn(name = "psc", referencedColumnName = "psc"),
            @JoinColumn(name = "cp", referencedColumnName = "cp")
    })
    private CukrarnaEntity cukrarna;

    @Id
    @ManyToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    private ZakaznikqlEntity zakaznik;

    public CukrarnaEntity getCukrarna() {
        return cukrarna;
    }

    public void setCukrarna(CukrarnaEntity cukrarna) {
        this.cukrarna = cukrarna;
    }

    public ZakaznikqlEntity getZakaznik() {
        return zakaznik;
    }

    public void setZakaznik(ZakaznikqlEntity zakaznik) {
        this.zakaznik = zakaznik;
    }

    // Additional fields, methods, and annotations as needed...

    @Override
    public int hashCode() {
        return Objects.hash(cukrarna, zakaznik);
    }
}
