package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.util.Objects;

import java.io.Serializable;
import java.util.Objects;

public class CukrarnaHasZakaznikEntityPK implements Serializable {

    private CukrarnaEntity cukrarna;
    private ZakaznikqlEntity zakaznik;

    public CukrarnaHasZakaznikEntityPK() {
    }

    public CukrarnaHasZakaznikEntityPK(CukrarnaEntity cukrarna, ZakaznikqlEntity zakaznik) {
        this.cukrarna = cukrarna;
        this.zakaznik = zakaznik;
    }

    public CukrarnaEntity getCukrarna() {
        return cukrarna;
    }

    public void setCukrarna(CukrarnaEntity cukrarna) {
        this.cukrarna = cukrarna;
    }

    public ZakaznikqlEntity getZakaznik() {
        return zakaznik;
    }

    public void setZakaznik(ZakaznikqlEntity zakaznik) {
        this.zakaznik = zakaznik;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CukrarnaHasZakaznikEntityPK)) return false;
        CukrarnaHasZakaznikEntityPK that = (CukrarnaHasZakaznikEntityPK) o;
        return Objects.equals(getCukrarna(), that.getCukrarna()) &&
                Objects.equals(getZakaznik(), that.getZakaznik());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCukrarna(), getZakaznik());
    }
}
