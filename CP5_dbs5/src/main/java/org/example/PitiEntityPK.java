package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

@Embeddable
public class PitiEntityPK implements Serializable {
    public PitiEntityPK() {
    }

    @Column(name = "verze", nullable = false)
    private BigDecimal nabidka;

    @Column(name = "nazev", nullable = false, length = 50)
    private String nazev;

    @Column(name = "velikost", nullable = false, length = 50)
    private String velikost;

    public BigDecimal getNabidka() {
        return nabidka;
    }

    public void setNabidka(BigDecimal nabidka) {
        this.nabidka = nabidka;
    }

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public String getVelikost() {
        return velikost;
    }

    public void setVelikost(String velikost) {
        this.velikost = velikost;
    }


    @Override
    public int hashCode() {
        return Objects.hash(nabidka, nazev, velikost);
    }
}
