package org.example.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.ClovekqlEntity;

import java.util.List;

public class ClovekqlDAO {

    private EntityManagerFactory emf;

    public ClovekqlDAO() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    public void close() {
        emf.close();
    }

    public void create(ClovekqlEntity clovekql) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(clovekql);
            clovekql.setId(809270);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public ClovekqlEntity read(int id) {
        EntityManager em = emf.createEntityManager();
        ClovekqlEntity clovekql = null;

        try {
            clovekql = em.find(ClovekqlEntity.class, id);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return clovekql;
    }

    public void update(ClovekqlEntity clovekql) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(clovekql);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void delete(ClovekqlEntity clovekql) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            clovekql = em.merge(clovekql);
            em.remove(clovekql);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public List<ClovekqlEntity> getAll() {
        EntityManager em = emf.createEntityManager();
        List<ClovekqlEntity> resultList = null;

        try {
            resultList = em.createQuery("SELECT c FROM ClovekqlEntity c", ClovekqlEntity.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return resultList;
    }

    public static void main(String[] args) {
        ClovekqlDAO clovekqlDAO = new ClovekqlDAO();

        // Creating a new ClovekqlEntity object
        ClovekqlEntity clovekql = new ClovekqlEntity();
        clovekql.setId(873948);

        clovekqlDAO.create(clovekql);
        System.out.println("ClovekqlEntity created: " + clovekql);

        // Reading a ClovekqlEntity object by its ID
        int id = 873948;
        ClovekqlEntity retrievedClovekql = clovekqlDAO.read(id);
        System.out.println("Retrieved ClovekqlEntity: " + retrievedClovekql);

        // Updating an existing ClovekqlEntity object
        retrievedClovekql.setId(982749);
        clovekqlDAO.update(retrievedClovekql);
        System.out.println("ClovekqlEntity updated: " + retrievedClovekql);

        // Deleting a ClovekqlEntity object
        clovekqlDAO.delete(retrievedClovekql);
        System.out.println("ClovekqlEntity deleted");

        // Getting all ClovekqlEntity objects
        List<ClovekqlEntity> allClovekqls = clovekqlDAO.getAll();
        System.out.println("All ClovekqlEntity objects:");
        for (ClovekqlEntity clovekqlEntity : allClovekqls) {
            System.out.println(clovekqlEntity);
        }

        ClovekqlEntity clovek = new ClovekqlEntity();
        clovek.setId(918483);
        // Set other fields with valid values
        // clovek.setFieldName(value);

        clovekqlDAO.create(clovek);


        clovekqlDAO.close();
    }
}
