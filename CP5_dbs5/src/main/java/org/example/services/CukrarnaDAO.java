package org.example.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.CukrarnaEntity;
import org.example.CukrarnaEntityPK;

import java.util.List;

public class CukrarnaDAO {

    private EntityManagerFactory emf;

    public CukrarnaDAO() {
        emf = Persistence.createEntityManagerFactory("yourPersistenceUnitName"); // Replace with your persistence unit name
    }

    public void close() {
        emf.close();
    }

    public void create(CukrarnaEntity cukrárna) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(cukrárna);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public CukrarnaEntity read(String ulice, int psc, int cp) {
        EntityManager em = emf.createEntityManager();
        CukrarnaEntity cukrárna = null;

        try {
            // Create a composite key object
            CukrarnaEntityPK key = new CukrarnaEntityPK(ulice, psc, cp);
            cukrárna = em.find(CukrarnaEntity.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return cukrárna;
    }

    public void update(CukrarnaEntity cukrárna) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(cukrárna);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void delete(CukrarnaEntity cukrarna) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            cukrarna = em.merge(cukrarna);
            em.remove(cukrarna);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public List<CukrarnaEntity> getAll() {
        EntityManager em = emf.createEntityManager();
        List<CukrarnaEntity> resultList = null;

        try {
            resultList = em.createQuery("SELECT c FROM CukrarnaEntity c", CukrarnaEntity.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return resultList;
    }
}
