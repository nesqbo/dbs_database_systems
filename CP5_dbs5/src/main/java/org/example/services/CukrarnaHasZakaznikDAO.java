package org.example.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.CukrarnaHasZakaznikEntity;
import org.example.CukrarnaHasZakaznikEntityPK;

import java.util.List;

public class CukrarnaHasZakaznikDAO {

    private EntityManagerFactory emf;
    public CukrarnaHasZakaznikDAO() {
        emf = Persistence.createEntityManagerFactory("default"); // Replace with your persistence unit name
    }

    public void close() {
        emf.close();
    }

    public void create(CukrarnaHasZakaznikEntity entity) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(entity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public CukrarnaHasZakaznikEntity read(String ulice, int psc, int cp, int id) {
        EntityManager em = emf.createEntityManager();
        CukrarnaHasZakaznikEntity entity = null;

        try {
            CukrarnaHasZakaznikEntityPK key = new CukrarnaHasZakaznikEntityPK();
            entity = em.find(CukrarnaHasZakaznikEntity.class, key);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return entity;
    }

    public void update(CukrarnaHasZakaznikEntity entity) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(entity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void delete(CukrarnaHasZakaznikEntity entity) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            entity = em.merge(entity);
            em.remove(entity);
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public List<CukrarnaHasZakaznikEntity> getAll() {
        EntityManager em = emf.createEntityManager();
        List<CukrarnaHasZakaznikEntity> resultList = null;

        try {
            resultList = em.createQuery("SELECT c FROM CukrarnaHasZakaznikEntity c", CukrarnaHasZakaznikEntity.class)
                    .getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            em.close();
        }

        return resultList;
    }
}
