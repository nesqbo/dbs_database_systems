package org.example.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.CukrarnaEntityPK;
import org.example.DodavatelEntityPK;
import org.example.HasDodavatelEntity;
import org.example.HasDodavatelEntityPK;

public class HasDodavatelDAO {
    private EntityManagerFactory emf;

    public HasDodavatelDAO() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    public void create(HasDodavatelEntity hasDodavatel) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(hasDodavatel);
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void update(HasDodavatelEntity hasDodavatel) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(hasDodavatel);
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void delete(HasDodavatelEntity hasDodavatel) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.remove(em.merge(hasDodavatel));
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

//    public HasDodavatelEntity findById(DodavatelEntityPK dodavatelPK, CukrarnaEntityPK cukrarnaPK) {
//        EntityManager em = emf.createEntityManager();
//
////        try {
////            return em.find(HasDodavatelEntity.class, new HasDodavatelEntityPK(dodavatelPK, cukrarnaPK));
////        } finally {
////            em.close();
////        }
//    }
}
