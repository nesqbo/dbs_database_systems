package org.example.services;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.JidloEntity;
import org.example.JidloEntityPK;

import java.math.BigDecimal;

public class JidloDAO {

    private EntityManagerFactory emf;

    public JidloDAO() {
        emf = Persistence.createEntityManagerFactory("default");
    }

    public void create(JidloEntity jidlo) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(jidlo);
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void update(JidloEntity jidlo) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.merge(jidlo);
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

    public void delete(JidloEntity jidlo) {
        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.remove(em.merge(jidlo));
            tx.commit();
        } catch (Exception e) {
            if (tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
    }

//    public JidloEntity findById(BigDecimal nabidka, String nazev) {
//        EntityManager em = emf.createEntityManager();
//
//        try {
//            return em.find(JidloEntity.class, new JidloEntityPK(0.5, "pomoc"));
//        } finally {
//            em.close();
//        }
//    }

    public static void main(String[] args) {
        JidloDAO jidloDAO = new JidloDAO();

        // Creating a new JidloEntity object
        JidloEntity jidlo = new JidloEntity();
        jidlo.setNabidka(new BigDecimal("5.39"));
        jidlo.setNazev("Pizza");

        jidloDAO.create(jidlo);
        System.out.println("JidloEntity created: " + jidlo);

        // Updating an existing JidloEntity object
        jidlo.setNabidka(new BigDecimal("7.77"));
        jidloDAO.update(jidlo);
        System.out.println("JidloEntity updated: " + jidlo);

        // Deleting an existing JidloEntity object
        jidloDAO.delete(jidlo);
        System.out.println("JidloEntity deleted: " + jidlo);

        // Finding a JidloEntity object by its composite primary key
        BigDecimal nabidka = new BigDecimal("7.77");
        String nazev = "Car";

//        JidloEntity foundJidlo = jidloDAO.findById(nabidka, nazev);
//        System.out.println("Found JidloEntity: " + foundJidlo);
    }
}
