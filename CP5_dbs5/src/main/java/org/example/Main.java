package org.example;

import jakarta.persistence.EntityManager;
import jakarta.persistence.EntityManagerFactory;
import jakarta.persistence.EntityTransaction;
import jakarta.persistence.Persistence;
import org.example.services.ClovekqlDAO;
import org.example.services.CukrarnaHasZakaznikDAO;
import org.example.services.HasDodavatelDAO;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Main {
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("default"); // Replace with your persistence unit name;

    private static ClovekqlDAO clovekqlDAO = new ClovekqlDAO();
    private static CukrarnaHasZakaznikDAO hasZakaznikDAO= new CukrarnaHasZakaznikDAO();
    private static CukrarnaHasZakaznikEntity hasZakaznik = new CukrarnaHasZakaznikEntity();
    private static ZakaznikqlEntity zakaznikqlEntity = new ZakaznikqlEntity();
    private static CukrarnaEntity cukrarna = new CukrarnaEntity();
    private static ClovekqlEntity clovekql = new ClovekqlEntity();
    private static CukrarnaHasZakaznikEntity cukrarnaHasZakaznik = new CukrarnaHasZakaznikEntity();
    public static void main(String[] args) throws ClassNotFoundException, SQLException {

        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection(
                "jdbc:postgresql://slon.felk.cvut.cz:5432/levaness",
                "levaness",
                "Korunomasawako1"
        );
        if(c!=null){
            System.out.println("Připojeno");
        }
        ClovekqlEntity clovekql = clovekqlDAO.read(100000);
        System.out.println("clovek: " + clovekql);

//        handleServices();
//        cukrarnaHasZakaznik();
        hasDodavatel();

    }

    public static void handleServices(){

        ClovekqlEntity clovek = new ClovekqlEntity();
        clovek.setId(983720);
        System.out.println(clovek);

        clovekqlDAO.create(clovek);

    }

    public static void cukrarnaHasZakaznik(){
        cukrarna.setUlice("hradecka");
        cukrarna.setPsc(38001);
        cukrarna.setCp(9);
        clovekql.setId(900000);

        zakaznikqlEntity.setZakaznickýUcet(false);


        EntityManager em = emf.createEntityManager();
        EntityTransaction tx = em.getTransaction();

        try {
            tx.begin();
            em.persist(cukrarnaHasZakaznik);
            System.out.println("ahoj");
            tx.commit();
        } catch (Exception e) {
            if (tx != null && tx.isActive()) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            em.close();
        }
//        CukrarnaHasZakaznikEntity kava = hasZakaznikDAO.read("hradecka", 38001, 9, 100000);
//        if (kava!=null){
//            System.out.println("jsem tu mami");
//        } else {
//            System.out.println("cry");
//        }
    }

    public static EntityManagerFactory getEmf() {
        return emf;
    }

    public static void setEmf(EntityManagerFactory emf) {
        Main.emf = emf;
    }

    public static ClovekqlDAO getClovekqlDAO() {
        return clovekqlDAO;
    }

    public static void setClovekqlDAO(ClovekqlDAO clovekqlDAO) {
        Main.clovekqlDAO = clovekqlDAO;
    }

    public static CukrarnaHasZakaznikDAO getHasZakaznikDAO() {
        return hasZakaznikDAO;
    }

    public static void setHasZakaznikDAO(CukrarnaHasZakaznikDAO hasZakaznikDAO) {
        Main.hasZakaznikDAO = hasZakaznikDAO;
    }

    public static CukrarnaHasZakaznikEntity getHasZakaznik() {
        return hasZakaznik;
    }

    public static void setHasZakaznik(CukrarnaHasZakaznikEntity hasZakaznik) {
        Main.hasZakaznik = hasZakaznik;
    }

    public static ZakaznikqlEntity getZakaznikqlEntity() {
        return zakaznikqlEntity;
    }

    public static void setZakaznikqlEntity(ZakaznikqlEntity zakaznikqlEntity) {
        Main.zakaznikqlEntity = zakaznikqlEntity;
    }

    public static CukrarnaEntity getCukrarna() {
        return cukrarna;
    }

    public static void setCukrarna(CukrarnaEntity cukrarna) {
        Main.cukrarna = cukrarna;
    }

    public static ClovekqlEntity getClovekql() {
        return clovekql;
    }

    public static void setClovekql(ClovekqlEntity clovekql) {
        Main.clovekql = clovekql;
    }

    public static CukrarnaHasZakaznikEntity getCukrarnaHasZakaznik() {
        return cukrarnaHasZakaznik;
    }

    public static void setCukrarnaHasZakaznik(CukrarnaHasZakaznikEntity cukrarnaHasZakaznik) {
        Main.cukrarnaHasZakaznik = cukrarnaHasZakaznik;
    }

    public static void cukrarnaHiresStaff(){

    }

    public static void hasDodavatel(){
        HasDodavatelDAO hasDodavatelDAO = new HasDodavatelDAO();

        // Creating a new HasDodavatelEntity object
        HasDodavatelEntity hasDodavatel = new HasDodavatelEntity();
        // Set the necessary properties

        hasDodavatelDAO.create(hasDodavatel);
        System.out.println("HasDodavatelEntity created: " + hasDodavatel);

        // Updating an existing HasDodavatelEntity object
        hasDodavatelDAO.update(hasDodavatel);
        System.out.println("HasDodavatelEntity updated: " + hasDodavatel);

        // Deleting an existing HasDodavatelEntity object
        hasDodavatelDAO.delete(hasDodavatel);
        System.out.println("HasDodavatelEntity deleted: " + hasDodavatel);

        // Finding a HasDodavatelEntity object by its composite primary key
        DodavatelEntityPK dodavatelPK = new DodavatelEntityPK();
        CukrarnaEntityPK cukrarnaPK = new CukrarnaEntityPK();
        // Set the necessary properties of the primary keys

//        HasDodavatelEntity foundHasDodavatel = hasDodavatelDAO.findById(dodavatelPK, cukrarnaPK);
//        System.out.println("Found HasDodavatelEntity: " + foundHasDodavatel);

    }


}