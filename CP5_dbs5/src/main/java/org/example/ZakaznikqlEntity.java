package org.example;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "zakaznikql", schema = "public", catalog = "levaness")
public class ZakaznikqlEntity {
    public ZakaznikqlEntity() {
    }

    @Id
    @OneToOne
    @JoinColumn(name = "id", referencedColumnName = "id")
    private ClovekqlEntity clovekql;
    @Basic
    @Column(name = "zakaznický_ucet", nullable = false)
    private boolean zakaznickýUcet;

    public boolean isZakaznickýUcet() {
        return zakaznickýUcet;
    }

    public void setZakaznickýUcet(boolean zakaznickýUcet) {
        this.zakaznickýUcet = zakaznickýUcet;
    }

    public ClovekqlEntity getClovekql() {
        return clovekql;
    }

    public void setClovekql(ClovekqlEntity clovekql) {
        this.clovekql = clovekql;
    }

    @Override
    public int hashCode() {
        return Objects.hash(clovekql, zakaznickýUcet);
    }
}
