package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "nabidka")
public class NabidkaEntity {
    public NabidkaEntity() {
    }

    @Id
    @Column(name = "verze", nullable = false, precision = 1)
    private BigDecimal verze;
    @Basic
    @Column(name = "jazyk", nullable = false, length = 50)
    private String jazyk;

    public BigDecimal getVerze() {
        return verze;
    }

    public void setVerze(BigDecimal verze) {
        this.verze = verze;
    }

    public String getJazyk() {
        return jazyk;
    }

    public void setJazyk(String jazyk) {
        this.jazyk = jazyk;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        NabidkaEntity that = (NabidkaEntity) o;
        return Objects.equals(verze, that.verze) && Objects.equals(jazyk, that.jazyk);
    }

    @Override
    public int hashCode() {
        return Objects.hash(verze, jazyk);
    }
}
