package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "cukrarna_offers_nabidka", schema = "public", catalog = "levaness")
@IdClass(CukrarnaOffersNabidkaEntityPK.class)
public class CukrarnaOffersNabidkaEntity {
    public CukrarnaOffersNabidkaEntity() {
    }
    @Id
    @OneToOne
    @JoinColumns({
            @JoinColumn(name = "ulice", referencedColumnName = "ulice"),
            @JoinColumn(name = "psc", referencedColumnName = "psc"),
            @JoinColumn(name = "cp", referencedColumnName = "cp")
    })
    private CukrarnaEntity cukrarna;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal verze;

    public CukrarnaEntity getCukrarna() {
        return cukrarna;
    }

    public void setCukrarna(CukrarnaEntity cukrarna) {
        this.cukrarna = cukrarna;
    }

    public BigDecimal getVerze() {
        return verze;
    }

    public void setVerze(BigDecimal verze) {
        this.verze = verze;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CukrarnaOffersNabidkaEntity that = (CukrarnaOffersNabidkaEntity) o;
        return Objects.equals(cukrarna, that.cukrarna) && Objects.equals(verze, that.verze);
    }

    @Override
    public int hashCode() {
        return Objects.hash(cukrarna, verze);
    }
}
