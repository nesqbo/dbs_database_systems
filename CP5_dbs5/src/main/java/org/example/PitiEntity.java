package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "piti")
public class PitiEntity {
    public PitiEntity() {
    }

    @EmbeddedId
    private PitiEntityPK pitiEntityPK;

    @Basic
    @Column(name = "velikost", nullable = false, length = 50)
    private String velikost;

    public String getVelikost() {
        return velikost;
    }

    public void setVelikost(String velikost) {
        this.velikost = velikost;
    }

    public PitiEntityPK getPitiEntityPK() {
        return pitiEntityPK;
    }

    public void setPitiEntityPK(PitiEntityPK pitiEntityPK) {
        this.pitiEntityPK = pitiEntityPK;
    }

    @Override
    public int hashCode() {
        return Objects.hash(pitiEntityPK, velikost);
    }
}
