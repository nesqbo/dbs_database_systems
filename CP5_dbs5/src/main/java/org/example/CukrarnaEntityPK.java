package org.example;

import jakarta.persistence.Column;
import jakarta.persistence.Id;

import java.io.Serializable;
import java.util.Objects;

public class CukrarnaEntityPK implements Serializable {

    public CukrarnaEntityPK() {
    }

    public CukrarnaEntityPK(String ulice, int psc, int cp) {
        this.ulice = ulice;
        this.psc = psc;
        this.cp = cp;
    }

    @Column(name = "ulice", nullable = false, length = 50)
    @Id
    private String ulice;
    @Column(name = "psc", nullable = false)
    @Id
    private int psc;
    @Column(name = "cp", nullable = false)
    @Id
    private int cp;

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CukrarnaEntityPK that = (CukrarnaEntityPK) o;
        return psc == that.psc && cp == that.cp && Objects.equals(ulice, that.ulice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ulice, psc, cp);
    }
}
