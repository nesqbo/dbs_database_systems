package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class DodavatelOffersNabidkaEntityPK implements Serializable {
    public DodavatelOffersNabidkaEntityPK() {
        }
    @Id
    @JoinColumn(name = "ico", referencedColumnName = "ico")
    private int dodavatel;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidka;

    public int getDodavatel() {
        return dodavatel;
    }

    public void setDodavatel(int dodavatel) {
        this.dodavatel = dodavatel;
    }

    public BigDecimal getNabidka() {
        return nabidka;
    }

    public void setNabidka(BigDecimal nabidka) {
        this.nabidka = nabidka;
    }


    @Override
        public int hashCode() {
            return Objects.hash(dodavatel, nabidka);
        }
    }
