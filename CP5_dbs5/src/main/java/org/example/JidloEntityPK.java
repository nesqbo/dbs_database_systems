package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class JidloEntityPK implements Serializable {
    public JidloEntityPK() {
    }
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidka;

    @Id
    @Column(name = "nazev", nullable = false, length = 50)
    private String nazev;

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public BigDecimal getNabidka() {
        return nabidka;
    }

    public void setNabidka(BigDecimal nabidka) {
        this.nabidka = nabidka;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nabidka, nazev);
    }}
