package org.example;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "cukrárna")
@IdClass(CukrarnaEntityPK.class)
public class CukrarnaEntity {
    public CukrarnaEntity() {
    }

    public CukrarnaEntity(String ulice, int psc, int cp) {
        this.ulice = ulice;
        this.psc = psc;
        this.cp = cp;
    }

    @Id
    @Column(name = "ulice", nullable = false, length = 50)
    private String ulice;
    @Id
    @Column(name = "psc", nullable = false)
    private int psc;
    @Id
    @Column(name = "cp", nullable = false)
    private int cp;

    public String getUlice() {
        return ulice;
    }

    public void setUlice(String ulice) {
        this.ulice = ulice;
    }

    public int getPsc() {
        return psc;
    }

    public void setPsc(int psc) {
        this.psc = psc;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CukrarnaEntity that = (CukrarnaEntity) o;
        return psc == that.psc && cp == that.cp && Objects.equals(ulice, that.ulice);
    }

    @Override
    public int hashCode() {
        return Objects.hash(ulice, psc, cp);
    }
}
