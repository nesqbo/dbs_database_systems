package org.example;

import jakarta.persistence.*;

import java.util.Objects;

@Entity
@Table(name = "clovekql")
@Inheritance(strategy = InheritanceType.JOINED)
//discriminator column for parent and discvalue for
public class ClovekqlEntity {

    public ClovekqlEntity() {
    }

    public ClovekqlEntity(int id) {
        this.id = id;
    }

    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Id
    @Column(name = "id", nullable = false)
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ClovekqlEntity that = (ClovekqlEntity) o;
        return id == that.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "ClovekqlEntity{" +
                "id=" + id +
                '}';
    }
}
