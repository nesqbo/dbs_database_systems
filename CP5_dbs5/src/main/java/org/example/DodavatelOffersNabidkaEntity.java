package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "dodavatel_offers_nabidka")
@IdClass(DodavatelOffersNabidkaEntityPK.class)
public class DodavatelOffersNabidkaEntity {
    public DodavatelOffersNabidkaEntity() {
    }

    @Id
    @JoinColumn(name = "ico", referencedColumnName = "ico")
    private int dodavatel;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidka;

    public int getDodavatel() {
        return dodavatel;
    }

    public void setDodavatel(int dodavatel) {
        this.dodavatel = dodavatel;
    }

    public BigDecimal getNabidka() {
        return nabidka;
    }

    public void setNabidka(BigDecimal nabidka) {
        this.nabidka = nabidka;
    }


    @Override
    public int hashCode() {
        return Objects.hash(dodavatel, nabidka);
    }
}
