package org.example;

import jakarta.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "has_dodavatel")
@IdClass(HasDodavatelEntityPK.class)
public class HasDodavatelEntity {
    public HasDodavatelEntity() {
    }

    @Id
    private DodavatelEntityPK dodavatelEntityPK;

    @Id
    private CukrarnaEntityPK cukrarnaEntityPK;

    public DodavatelEntityPK getDodavatelEntityPK() {
        return dodavatelEntityPK;
    }

    public void setDodavatelEntityPK(DodavatelEntityPK dodavatelEntityPK) {
        this.dodavatelEntityPK = dodavatelEntityPK;
    }

    public CukrarnaEntityPK getCukrarnaEntityPK() {
        return cukrarnaEntityPK;
    }

    public void setCukrarnaEntityPK(CukrarnaEntityPK cukrarnaEntityPK) {
        this.cukrarnaEntityPK = cukrarnaEntityPK;
    }

    @Override
    public String toString() {
        return "HasDodavatelEntity{" +
                "dodavatelEntityPK=" + dodavatelEntityPK +
                ", cukrarnaEntityPK=" + cukrarnaEntityPK +
                '}';
    }
}
