package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "zakaznik_orders_from_nabidka", schema = "public", catalog = "levaness")
@IdClass(ZakaznikOrdersFromNabidkaEntityPK.class)
public class ZakaznikOrdersFromNabidkaEntity {
    public ZakaznikOrdersFromNabidkaEntity() {
    }

    @Id
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Integer zakaznik;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidkaEntity;

    public Integer getZakaznik() {
        return zakaznik;
    }

    public void setZakaznik(Integer zakaznik) {
        this.zakaznik = zakaznik;
    }

    public BigDecimal getNabidkaEntity() {
        return nabidkaEntity;
    }

    public void setNabidkaEntity(BigDecimal nabidkaEntity) {
        this.nabidkaEntity = nabidkaEntity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(zakaznik, nabidkaEntity);
    }
}
