package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "version5_nabidka", schema = "public", catalog = "levaness")
public class Version5NabidkaEntity {
    @Id
    @Column(name = "newest_verze", nullable = true, precision = 1)
    private BigDecimal newestVerze;
    @Basic
    @Column(name = "newest_jidlo", nullable = true, length = 50)
    private String newestJidlo;

    public BigDecimal getNewestVerze() {
        return newestVerze;
    }

    public void setNewestVerze(BigDecimal newestVerze) {
        this.newestVerze = newestVerze;
    }

    public String getNewestJidlo() {
        return newestJidlo;
    }

    public void setNewestJidlo(String newestJidlo) {
        this.newestJidlo = newestJidlo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Version5NabidkaEntity that = (Version5NabidkaEntity) o;
        return Objects.equals(newestVerze, that.newestVerze) && Objects.equals(newestJidlo, that.newestJidlo);
    }

    @Override
    public int hashCode() {
        return Objects.hash(newestVerze, newestJidlo);
    }
}
