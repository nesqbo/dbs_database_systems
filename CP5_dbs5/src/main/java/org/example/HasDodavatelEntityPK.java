package org.example;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Objects;

public class HasDodavatelEntityPK implements Serializable {
    public HasDodavatelEntityPK() {
    }

    @Id
    private DodavatelEntityPK dodavatelEntityPK;

    @Id
    private CukrarnaEntityPK cukrarnaEntityPK;

    public DodavatelEntityPK getDodavatelEntityPK() {
        return dodavatelEntityPK;
    }

    public void setDodavatelEntityPK(DodavatelEntityPK dodavatelEntityPK) {
        this.dodavatelEntityPK = dodavatelEntityPK;
    }

    public CukrarnaEntityPK getCukrarnaEntityPK() {
        return cukrarnaEntityPK;
    }

    public void setCukrarnaEntityPK(CukrarnaEntityPK cukrarnaEntityPK) {
        this.cukrarnaEntityPK = cukrarnaEntityPK;
    }

    @Override
    public int hashCode() {
        return Objects.hash(cukrarnaEntityPK, dodavatelEntityPK);
    }
}
