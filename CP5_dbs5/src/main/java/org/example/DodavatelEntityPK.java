package org.example;

import jakarta.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Embeddable
public class DodavatelEntityPK implements Serializable {
    public DodavatelEntityPK() {
    }

    @Column(name = "id", nullable = false)
    private int clovekql;

    @Column(name = "nazev", nullable = false, length = 50)
    private String nazev;

    @Column(name = "ico", nullable = false)
    private int ico;

    @Column(name = "sidlo", nullable = false, length = 50)
    private String sidlo;

    public String getNazev() {
        return nazev;
    }

    public void setNazev(String nazev) {
        this.nazev = nazev;
    }

    public int getIco() {
        return ico;
    }

    public void setIco(int ico) {
        this.ico = ico;
    }

    public String getSidlo() {
        return sidlo;
    }

    public void setSidlo(String sidlo) {
        this.sidlo = sidlo;
    }

    public int getClovekql() {
        return clovekql;
    }

    public void setClovekql(int clovekql) {
        this.clovekql = clovekql;
    }
}

