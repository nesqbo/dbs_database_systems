package org.example;

import jakarta.persistence.*;

import java.math.BigDecimal;
import java.util.Objects;

@Entity
@Table(name = "includes_jidlo")
@IdClass(IncludesJidloEntityPK.class)
public class IncludesJidloEntity {
    public IncludesJidloEntity() {
    }

    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidka;

    @Id
    @JoinColumn(name = "nazev", referencedColumnName = "nazev")
    private String jidlo;

    public BigDecimal getNabidka() {
        return nabidka;
    }

    public void setNabidka(BigDecimal nabidka) {
        this.nabidka = nabidka;
    }

    public String getJidlo() {
        return jidlo;
    }

    public void setJidlo(String jidlo) {
        this.jidlo = jidlo;
    }

    @Override
    public int hashCode() {
        return Objects.hash(nabidka, jidlo);
    }
}
