package org.example;

import jakarta.persistence.*;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

public class ZakaznikOrdersFromNabidkaEntityPK implements Serializable {
    public ZakaznikOrdersFromNabidkaEntityPK() {
    }


    @Id
    @JoinColumn(name = "id", referencedColumnName = "id")
    private Integer zakaznik;
    @Id
    @JoinColumn(name = "verze", referencedColumnName = "verze")
    private BigDecimal nabidkaEntity;

    public Integer getZakaznik() {
        return zakaznik;
    }

    public void setZakaznik(Integer zakaznik) {
        this.zakaznik = zakaznik;
    }

    public BigDecimal getNabidkaEntity() {
        return nabidkaEntity;
    }

    public void setNabidkaEntity(BigDecimal nabidkaEntity) {
        this.nabidkaEntity = nabidkaEntity;
    }

    @Override
    public int hashCode() {
        return Objects.hash(zakaznik, nabidkaEntity);
    }
}
