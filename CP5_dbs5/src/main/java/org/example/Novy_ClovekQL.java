package org.example;

public class Novy_ClovekQL extends ClovekqlEntity{
    public Novy_ClovekQL() {
        super();
    }

    public Novy_ClovekQL(int id) {
        super(id);
    }

    @Override
    public int getId() {
        return super.getId();
    }

    @Override
    public void setId(int id) {
        super.setId(id);
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
