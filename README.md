# DBS_database_systems



## semestral work
This repository contains the work i have done during the summer semester of 2023.<br><br>
Work done:
<table>
    <thead>
        <tr>
            <th><strong>HW</strong></th>
            <th><strong>Short descr.</strong></th>
            <th><strong>Description</strong></th>
        </tr>
    </thead>
        <tr>
            <td><strong>CP1</strong></td>
            <td>Creation of ER model with different kinds of connectors.</td>
            <td></td>
        </tr>
        <tr>
            <td><strong>CP2</strong></td>
            <td>Transformation of the ER model into the relational model</td>
            <td></td>
        </tr>
        <tr>
            <td><strong>CP3</strong></td>
            <td>Transformation of the relational model into an SQL database. Also created multiple querries.</td>
            <td></td>
        </tr>
        <tr>
            <td><strong>CP4</strong></td>
            <td>Advanced db technologies: index, view, trigger, transactions</td>
            <td></td>
        </tr>
        <tr>
            <td><strong>CP5</strong></td>
            <td>Connected my postgreSQL db with java through a JPA application</td>
            <td></td>
        </tr>
    <tfoot>
    </tfoot>
</table>

***